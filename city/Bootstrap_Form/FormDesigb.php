<!DOCTYPE html>
<html lang="en">
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assest/bootstrap/css/bootstrap.min.css">
    <script src="../../assest/bootstrap/js/jquery.min.js"></script>
    <script src="../../assest/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h5>Add your Favourite City</h5>

    <form class="form-inline">
        <div class="form-group">
            <label for="title">City Name</label>

        </div>

        <select class="btn-group-vertical">
            <option value="Dhaka">Dhaka</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Khulna">Khulna</option>
            <option value="Rajshahi">Rajshahi</option>
        </select>
        <button type="save" class="btn btn-group-sm" >Save</button>
    </form>
</div>

</body>
</html>

